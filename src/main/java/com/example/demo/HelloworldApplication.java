package com.example.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

import com.example.controllers.HomeController;


@SpringBootApplication
@ComponentScan(basePackageClasses = HomeController.class)
public class HelloworldApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(HelloworldApplication.class)
			.sources(HelloworldApplication.class)
			.run(args);
	}
}
