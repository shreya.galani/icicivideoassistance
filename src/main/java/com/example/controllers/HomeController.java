package com.example.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	
	@RequestMapping(method=RequestMethod.GET, value ="/")
	public String serveApp(
			@RequestParam("productCode") String productCode,
			@RequestParam("premiumAmount") String premiumAmount,
			@RequestParam("premiumDueDate") String premiumDueDate,
			@RequestParam("policyNumber") String policyNumber,
			HttpServletRequest request) {

		System.out.println("--->"+ productCode);
		
		return "index";
	}
}
